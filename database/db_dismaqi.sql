-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2020 at 08:07 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_dismaqi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_datarombel`
--

CREATE TABLE `tbl_datarombel` (
  `id_datarombel` int(11) NOT NULL,
  `kode_rombel` varchar(60) NOT NULL,
  `nis_siswa` varchar(60) NOT NULL,
  `kode_tahunajaran` varchar(60) NOT NULL,
  `kode_jenjang` varchar(60) DEFAULT NULL,
  `kode_kelas` varchar(60) DEFAULT NULL,
  `kode_jurusan` varchar(60) DEFAULT NULL,
  `tanggal_terdaftar` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_datarombel`
--

INSERT INTO `tbl_datarombel` (`id_datarombel`, `kode_rombel`, `nis_siswa`, `kode_tahunajaran`, `kode_jenjang`, `kode_kelas`, `kode_jurusan`, `tanggal_terdaftar`) VALUES
(13, 'X RPL 1718', '17180410005', '1718', '04-SMKI', '01-10', '01-RPL', '2020-09-02 23:54:05'),
(14, 'X RPL 1718', '17180410002', '1718', '04-SMKI', '01-10', '02-TKJ', '2020-09-02 23:54:05'),
(15, 'X RPL 1718', '17180410003', '1718', '04-SMKI', '01-10', '02-TKJ', '2020-09-02 23:54:05'),
(16, 'X RPL 1718', '17180410004', '1718', '04-SMKI', '01-10', '02-TKJ', '2020-09-02 23:54:05'),
(17, 'X RPL 1718', '17180410006', '1718', '04-SMKI', '01-10', '02-TKJ', '2020-09-02 23:54:05'),
(18, 'X RPL 1718', '17180410007', '1718', '04-SMKI', '01-10', '02-TKJ', '2020-09-02 23:54:05'),
(19, 'X RPL 1819', '17180410005', '1819', '04-SMKI', '02-11', '01-RPL', '2020-09-02 23:54:59'),
(20, 'X RPL 1819', '17180410002', '1819', '04-SMKI', '02-11', '02-TKJ', '2020-09-02 23:54:59'),
(21, 'X RPL 1819', '17180410003', '1819', '04-SMKI', '02-11', '02-TKJ', '2020-09-02 23:54:59'),
(22, 'X RPL 1819', '17180410004', '1819', '04-SMKI', '02-11', '02-TKJ', '2020-09-02 23:54:59'),
(23, 'X RPL 1819', '17180410006', '1819', '04-SMKI', '02-11', '02-TKJ', '2020-09-02 23:54:59'),
(24, 'X RPL 1819', '17180410007', '1819', '04-SMKI', '02-11', '02-TKJ', '2020-09-02 23:54:59'),
(25, 'X RPL 1920', '17180410005', '1920', '04-SMKI', '03-12', '01-RPL', '2020-09-02 23:59:03'),
(26, 'X RPL 1920', '17180410002', '1920', '04-SMKI', '03-12', '02-TKJ', '2020-09-02 23:59:03'),
(27, 'X RPL 1920', '17180410003', '1920', '04-SMKI', '03-12', '02-TKJ', '2020-09-02 23:59:03'),
(28, 'X RPL 1920', '17180410004', '1920', '04-SMKI', '03-12', '02-TKJ', '2020-09-02 23:59:03'),
(29, 'X RPL 1920', '17180410006', '1920', '04-SMKI', '03-12', '02-TKJ', '2020-09-02 23:59:03'),
(30, 'X RPL 1920', '17180410007', '1920', '04-SMKI', '03-12', '02-TKJ', '2020-09-02 23:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenjang`
--

CREATE TABLE `tbl_jenjang` (
  `kode_jenjang` varchar(60) NOT NULL,
  `jenjang` varchar(60) DEFAULT NULL,
  `descripsi_jenjang` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jenjang`
--

INSERT INTO `tbl_jenjang` (`kode_jenjang`, `jenjang`, `descripsi_jenjang`) VALUES
('01-TKIT', 'TKIT', 'TK Islam Terpadu Rabbaanii'),
('02-SDIT', 'SDIT', 'Sekolah Dasar Islam Terpadu Rabbaanii'),
('03-SMP', 'SMP', 'Sekolah Menengah Pertama Rabbaanii'),
('04-SMKI', 'SMKI', 'Sekolah Menengah Kejuruan Islam Rabbaanii');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jurusan`
--

CREATE TABLE `tbl_jurusan` (
  `kode_jurusan` varchar(60) NOT NULL,
  `jurusan` varchar(60) DEFAULT NULL,
  `descripsi_jurusan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jurusan`
--

INSERT INTO `tbl_jurusan` (`kode_jurusan`, `jurusan`, `descripsi_jurusan`) VALUES
('01-RPL', 'RPL', 'Rekayasa Perangkat Lunak / Software Engineer'),
('02-TKJ', 'TKJ', 'Teknik Komputer & Jaringan / Network Engineer'),
('03-MM', 'Multimedia', 'Multimedia / Broadcaster');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kelas`
--

CREATE TABLE `tbl_kelas` (
  `kode_kelas` varchar(60) NOT NULL,
  `kelas` varchar(60) DEFAULT NULL,
  `descripsi_kelas` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kelas`
--

INSERT INTO `tbl_kelas` (`kode_kelas`, `kelas`, `descripsi_kelas`) VALUES
('01-10', '10/X', 'Kelas 10'),
('02-11', '11/XI', 'Kelas 11'),
('03-12', '12/XII', 'Kelas 12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_signup`
--

CREATE TABLE `tbl_signup` (
  `kode_user` varchar(60) NOT NULL,
  `namalengkap_user` text DEFAULT NULL,
  `telepon_user` text DEFAULT NULL,
  `tanggal_lahiruser` text DEFAULT NULL,
  `username_user` text DEFAULT NULL,
  `password_user` text DEFAULT NULL,
  `userakses` text DEFAULT NULL,
  `tanggalregister` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_signup`
--

INSERT INTO `tbl_signup` (`kode_user`, `namalengkap_user`, `telepon_user`, `tanggal_lahiruser`, `username_user`, `password_user`, `userakses`, `tanggalregister`) VALUES
('GURU-0109201048', 'Asep Septiadi, S.Kom.', '081xxxxxxxxx', '21-09-1994', 'septiadi', '!!&21adi', 'GURU', '2020-09-05 05:35:22'),
('GURU-0509202019', 'Hendri Firmansyah', '081xxxx', '25 Oktober 1993', 'hendri', 'hendri', 'GURU', '2020-09-05 05:35:22'),
('GURU-0609202019', 'Heni Andriyani', '081xxxx', '12 Oktober 1993', 'heni', 'heni', 'GURU', '2020-09-05 05:35:49'),
('GURU-06092020191238', 'Jeni Firnanda', '085232xxxxx', '18 Juli 1994', 'jeni', 'jeni', 'GURU', '2020-09-05 05:39:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `nis_siswa` varchar(60) NOT NULL,
  `nama_siswa` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`nis_siswa`, `nama_siswa`) VALUES
('17180410002', 'Fatchan Muhammad Hakim'),
('17180410003', 'Muhammad Nur Irsaddul Maruf Brk'),
('17180410004', 'Muhamad Ridwan Alazzam'),
('17180410005', 'Raga Mutadha Muthahari'),
('17180410006', 'Ramadhani Bagus Satrio Wibowo'),
('17180410007', 'Ridho Fitra Palasa');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tahunajaran`
--

CREATE TABLE `tbl_tahunajaran` (
  `kode_tahunajaran` varchar(60) NOT NULL,
  `descripsi_ta` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_tahunajaran`
--

INSERT INTO `tbl_tahunajaran` (`kode_tahunajaran`, `descripsi_ta`) VALUES
('1718', 'TA. 2017/2018'),
('1819', 'TA. 2018/2019'),
('1920', 'TA. 2019/2020'),
('2021', 'TA. 2020/2021');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_datarombel`
--
ALTER TABLE `tbl_datarombel`
  ADD PRIMARY KEY (`id_datarombel`,`kode_rombel`,`nis_siswa`,`kode_tahunajaran`),
  ADD KEY `fk_ta2datarombel` (`kode_tahunajaran`),
  ADD KEY `fk_siswa2datarombel` (`nis_siswa`),
  ADD KEY `fk_jenjang2datarombel` (`kode_jenjang`),
  ADD KEY `fk_kelas2datarombel` (`kode_kelas`),
  ADD KEY `fk_jurusan2datarombel` (`kode_jurusan`);

--
-- Indexes for table `tbl_jenjang`
--
ALTER TABLE `tbl_jenjang`
  ADD PRIMARY KEY (`kode_jenjang`);

--
-- Indexes for table `tbl_jurusan`
--
ALTER TABLE `tbl_jurusan`
  ADD PRIMARY KEY (`kode_jurusan`);

--
-- Indexes for table `tbl_kelas`
--
ALTER TABLE `tbl_kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `tbl_signup`
--
ALTER TABLE `tbl_signup`
  ADD PRIMARY KEY (`kode_user`);

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`nis_siswa`);

--
-- Indexes for table `tbl_tahunajaran`
--
ALTER TABLE `tbl_tahunajaran`
  ADD PRIMARY KEY (`kode_tahunajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_datarombel`
--
ALTER TABLE `tbl_datarombel`
  MODIFY `id_datarombel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_datarombel`
--
ALTER TABLE `tbl_datarombel`
  ADD CONSTRAINT `fk_jenjang2datarombel` FOREIGN KEY (`kode_jenjang`) REFERENCES `tbl_jenjang` (`kode_jenjang`),
  ADD CONSTRAINT `fk_jurusan2datarombel` FOREIGN KEY (`kode_jurusan`) REFERENCES `tbl_jurusan` (`kode_jurusan`),
  ADD CONSTRAINT `fk_kelas2datarombel` FOREIGN KEY (`kode_kelas`) REFERENCES `tbl_kelas` (`kode_kelas`),
  ADD CONSTRAINT `fk_siswa2datarombel` FOREIGN KEY (`nis_siswa`) REFERENCES `tbl_siswa` (`nis_siswa`),
  ADD CONSTRAINT `fk_ta2datarombel` FOREIGN KEY (`kode_tahunajaran`) REFERENCES `tbl_tahunajaran` (`kode_tahunajaran`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
