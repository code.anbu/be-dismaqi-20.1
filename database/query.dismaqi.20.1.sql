CREATE DATABASE db_dismaqi;
USE db_dismaqi;
SHOW TABLES;
SHOW DATABASES;
USE test;
DROP DATABASE db_dismaqi;

-- TABLE JENJANG --
CREATE TABLE tbl_jenjang
(
    kode_jenjang VARCHAR(60) NOT NULL,
    jenjang VARCHAR(60),
    descripsi_jenjang TEXT,
    CONSTRAINT pk_jenjang PRIMARY KEY (kode_jenjang)
);

DESC tbl_jenjang;   

ALTER TABLE tbl_jenjang
ADD COLUMN descripsi_jenjang TEXT;

-- TAMBAH JENJANG --
INSERT INTO tbl_jenjang
(kode_jenjang, jenjang, descripsi_jenjang)
VALUES
('01-TKIT', 'TKIT', 'TK Islam Terpadu Rabbaanii'),
('02-SDIT', 'SDIT', 'Sekolah Dasar Islam Terpadu Rabbaanii'),
('03-SMP', 'SMP', 'Sekolah Menengah Pertama Rabbaanii'),
('04-SMKI', 'SMKI', 'Sekolah Menengah Kejuruan Islam Rabbaanii');

SELECT * FROM tbl_jenjang;

DROP TABLE tbl_jenjang;

-- TABLE KELAS --
CREATE TABLE tbl_kelas
( 
    kode_kelas VARCHAR(60) NOT NULL,
    kelas VARCHAR(60),
    descripsi_kelas TEXT,
    CONSTRAINT pk_kelas PRIMARY KEY (kode_kelas)
);

DESC tbl_kelas;
DROP TABLE tbl_kelas;

-- TAMBAH KELAS SMK --
INSERT INTO tbl_kelas
(kode_kelas, kelas, descripsi_kelas)
VALUES
('01-10', '10/X', 'Kelas 10'),
('02-11', '11/XI', 'Kelas 11'),
('03-12', '12/XII', 'Kelas 12');

SELECT * FROM tbl_kelas;

-- TABLE JURUSAN --
CREATE TABLE tbl_jurusan
(
    kode_jurusan VARCHAR(60) NOT NULL,
    jurusan VARCHAR(60),
    descripsi_jurusan TEXT,
    CONSTRAINT pk_jurusan PRIMARY KEY (kode_jurusan)
);

DESC tbl_jurusan;

-- TAMBAH JURUSAN --
INSERT INTO tbl_jurusan
(kode_jurusan, jurusan, descripsi_jurusan)
VALUES
('01-RPL', 'RPL', 'Rekayasa Perangkat Lunak / Software Engineer'),
('02-TKJ', 'TKJ', 'Teknik Komputer & Jaringan / Network Engineer'),
('03-MM', 'Multimedia', 'Multimedia / Broadcaster');

SELECT * FROM tbl_jurusan;

-- TABLE SISWA --
CREATE TABLE tbl_siswa
(
    nis_siswa VARCHAR(60) PRIMARY KEY,
    nama_siswa TEXT
)

DESC tbl_siswa;

INSERT INTO tbl_siswa
(nis_siswa, nama_siswa)
VALUES
('17180410005', 'Raga Mutadha Muthahari'),
('17180410002', 'Fatchan Muhammad Hakim'),
('17180410003', 'Muhammad Nur Irsaddul Maruf Brk'),
('17180410004', 'Muhamad Ridwan Alazzam'),
('17180410006', 'Ramadhani Bagus Satrio Wibowo'),
('17180410007', 'Ridho Fitra Palasa');

SELECT * FROM tbl_siswa;

CREATE TABLE tbl_datarombel
(
    id_datarombel INT(11) NOT NULL AUTO_INCREMENT,
    kode_rombel VARCHAR(60) NOT NULL,
    nis_siswa VARCHAR(60) NOT NULL,
    kode_tahunajaran VARCHAR(60) NOT NULL,
    kode_jenjang VARCHAR(60),
    kode_kelas VARCHAR(60),
    kode_jurusan VARCHAR(60),
    tanggal_terdaftar TIMESTAMP,
    CONSTRAINT pk_datarombel PRIMARY KEY (id_datarombel, kode_rombel, nis_siswa, kode_tahunajaran)
);

-- TAMBAH DATA SISWA --
-- 2017/2018 --
INSERT INTO tbl_datarombel
(kode_rombel, nis_siswa, kode_tahunajaran, kode_jenjang, kode_kelas, kode_jurusan)
VALUES
('X RPL 1718', '17180410005', '1718', '04-SMKI', '01-10', '01-RPL'),
('X RPL 1718', '17180410002', '1718', '04-SMKI', '01-10', '02-TKJ'),
('X RPL 1718', '17180410003', '1718', '04-SMKI', '01-10', '02-TKJ'),
('X RPL 1718', '17180410004', '1718', '04-SMKI', '01-10', '02-TKJ'),
('X RPL 1718', '17180410006', '1718', '04-SMKI', '01-10', '02-TKJ'),
('X RPL 1718', '17180410007', '1718', '04-SMKI', '01-10', '02-TKJ');

-- 2018/2019 --
INSERT INTO tbl_datarombel
(kode_rombel, nis_siswa, kode_tahunajaran, kode_jenjang, kode_kelas, kode_jurusan)
VALUES
('X RPL 1819', '17180410005', '1819', '04-SMKI', '02-11', '01-RPL'),
('X RPL 1819', '17180410002', '1819', '04-SMKI', '02-11', '02-TKJ'),
('X RPL 1819', '17180410003', '1819', '04-SMKI', '02-11', '02-TKJ'),
('X RPL 1819', '17180410004', '1819', '04-SMKI', '02-11', '02-TKJ'),
('X RPL 1819', '17180410006', '1819', '04-SMKI', '02-11', '02-TKJ'),
('X RPL 1819', '17180410007', '1819', '04-SMKI', '02-11', '02-TKJ');

-- 2019/2020 --
INSERT INTO tbl_datarombel
(kode_rombel, nis_siswa, kode_tahunajaran, kode_jenjang, kode_kelas, kode_jurusan)
VALUES
('X RPL 1920', '17180410005', '1920', '04-SMKI', '03-12', '01-RPL'),
('X RPL 1920', '17180410002', '1920', '04-SMKI', '03-12', '02-TKJ'),
('X RPL 1920', '17180410003', '1920', '04-SMKI', '03-12', '02-TKJ'),
('X RPL 1920', '17180410004', '1920', '04-SMKI', '03-12', '02-TKJ'),
('X RPL 1920', '17180410006', '1920', '04-SMKI', '03-12', '02-TKJ'),
('X RPL 1920', '17180410007', '1920', '04-SMKI', '03-12', '02-TKJ');

SELECT * FROM tbl_datarombel;

SELECT COUNT(*) 
FROM tbl_datarombel
WHERE kode_rombel = 'X RPL 1718';

DELETE FROM tbl_datarombel;
DESC tbl_datarombel;
DROP TABLE tbl_siswa;

-- TABLEL TAHUN AJARAN --
CREATE TABLE tbl_tahunajaran
(
    kode_tahunajaran VARCHAR(60) NOT NULL,
    descripsi_ta TEXT,
    CONSTRAINT pk_tahunajaran PRIMARY KEY (kode_tahunajaran)
);

DESC tbl_tahunajaran;

-- TAMBAH TAHUN AJARAN --
INSERT INTO tbl_tahunajaran
(kode_tahunajaran, descripsi_ta)
VALUES
('1718', 'TA. 2017/2018'),
('1819', 'TA. 2018/2019'),
('1920', 'TA. 2019/2020'),
('2021', 'TA. 2020/2021');

DROP TABLE tbl_tahunajaran;

SELECT * FROM tbl_tahunajaran;
SELECT * FROM tbl_jenjang;
SELECT * FROM tbl_kelas;
SELECT * FROM tbl_jurusan;

('1711005', 'Raga Mutadha Muthahari', '1718', '04-SMKI', '10/X', '01-RPL');


DELETE FROM tbl_siswa;

ALTER TABLE tbl_siswa
ADD COLUMN id_siswa INT(11) BEGIN nis_siswa

DESC tbl_siswa;

SELECT * FROM tbl_siswa;

-- RELASI ANTAR TABLE --
USE db_dismaqi;
SHOW TABLES;

-- RELASI TABLE TAHUN AJARAN KE TABLE SISWA --

ALTER TABLE tbl_siswa DROP FOREIGN KEY fk_ta2siswa;

ALTER TABLE tbl_datarombel
ADD CONSTRAINT fk_ta2datarombel
FOREIGN KEY (kode_tahunajaran) REFERENCES tbl_tahunajaran (kode_tahunajaran);

ALTER TABLE tbl_datarombel
ADD CONSTRAINT fk_siswa2datarombel
FOREIGN KEY (nis_siswa) REFERENCES tbl_siswa (nis_siswa);

ALTER TABLE tbl_datarombel
ADD CONSTRAINT fk_jenjang2datarombel
FOREIGN KEY (kode_jenjang) REFERENCES tbl_jenjang (kode_jenjang);

ALTER TABLE tbl_datarombel
ADD CONSTRAINT fk_kelas2datarombel
FOREIGN KEY (kode_kelas) REFERENCES tbl_kelas (kode_kelas);

ALTER TABLE tbl_datarombel
ADD CONSTRAINT fk_jurusan2datarombel
FOREIGN KEY (kode_jurusan) REFERENCES tbl_jurusan (kode_jurusan);

-- RELASI TABLE KELAS KE TABLE SISWA --

ALTER TABLE tbl_siswa DROP FOREIGN KEY fk_kelas2siswa;

ALTER TABLE tbl_siswa
ADD CONSTRAINT fk_kelas2siswa
FOREIGN KEY (kode_kelas) REFERENCES tbl_kelas (kode_kelas);

-- RELASI TABLE JURUSAN KE TABLE SISWA --

ALTER TABLE tbl_siswa DROP FOREIGN KEY fk_jurusan2siswa;

ALTER TABLE tbl_siswa
ADD CONSTRAINT fk_jurusan2siswa
FOREIGN KEY (kode_jurusan) REFERENCES tbl_jurusan (kode_jurusan);

-- RELASI TABLE JENJANG KE TABLE SISWA --

ALTER TABLE tbl_siswa DROP FOREIGN KEY fk_jenjang2siswa;

ALTER TABLE tbl_siswa
ADD CONSTRAINT fk_jenjang2siswa
FOREIGN KEY (kode_jenjang) REFERENCES tbl_jenjang (kode_jenjang);

-- AUTHENTICATION / SIGN UP --
CREATE TABLE tbl_signup
(
    kode_user VARCHAR(60) PRIMARY KEY,
    namalengkap_user TEXT,
    telepon_user TEXT,
    tanggal_lahiruser TEXT,
    username_user TEXT,
    password_user TEXT,
    userakses TEXT
);

DESC tbl_signup;

-- PROSES SIGNUP / TAMBAH DATA & UNTUK PROSES SIGNIN --
INSERT INTO tbl_signup
(kode_user, namalengkap_user, telepon_user, tanggal_lahiruser, username_user, password_user, userakses)
VALUES
('GURU-0109201048', 'Asep Septiadi, S.Kom.', '081xxxxxxxxx', '21-09-1994', 'septiadi', '!!&21adi', 'GURU');

SELECT * FROM tbl_signup;

SELECT tbl_tahunajaran.kode_tahunajaran, tbl_jenjang.jenjang, tbl_datarombel.kode_rombel, tbl_siswa.nis_siswa, tbl_siswa.nama_siswa, tbl_kelas.kelas

FROM tbl_datarombel

JOIN tbl_tahunajaran ON tbl_datarombel.kode_tahunajaran = tbl_tahunajaran.kode_tahunajaran
JOIN tbl_jenjang ON tbl_datarombel.kode_jenjang = tbl_jenjang.kode_jenjang
JOIN tbl_kelas ON tbl_datarombel.kode_kelas = tbl_kelas.kode_kelas
JOIN tbl_jurusan ON tbl_datarombel.kode_jurusan = tbl_jurusan.kode_jurusan
JOIN tbl_siswa ON tbl_datarombel.nis_siswa = tbl_siswa.nis_siswa
 
WHERE tbl_tahunajaran.kode_tahunajaran = '1718'

SELECT * FROM tbl_signup WHERE username_user = 'septiadi'

INSERT INTO tbl_signup
(kode_user, namalengkap_user, telepon_user, tanggal_lahiruser, username_user, password_user, userakses)
VALUES
('GURU-0609202019', 'Heni Andriyani', '081xxxx', '12 Oktober 1993', 'heni', 'heni', 'GURU');

SELECT * FROM tbl_signup;

ALTER TABLE tbl_signup
ADD COLUMN tanggalregister TIMESTAMP AFTER userakses

SELECT * FROM tbl_signup WHERE username_user = 'hendri' AND password_user = 'hendri'